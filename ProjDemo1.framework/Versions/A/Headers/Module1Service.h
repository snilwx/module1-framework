//
//  Module1Service.h
//  ProjDemo1
//
//  Created by wxiang on 2020/9/15.
//  Copyright © 2020 wxiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Module1Service : NSObject
- (NSString *)serviceIntro;
@end

NS_ASSUME_NONNULL_END
