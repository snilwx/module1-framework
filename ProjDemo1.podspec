Pod::Spec.new do |spec|

  spec.name         = "ProjDemo1" # 私有库名称
  spec.version      = "1.0.5" # 私有库版本号
  spec.summary      = "Module1 framework" # 项目简介
  spec.description  = "这是一个pods私有库的Module1的framework" # 项目简介

  spec.homepage     = "https://gitee.com/snilwx/module1-framework" # 仓库的主页

  spec.license      = "MIT" # 开源许可证

  spec.author       = { "wangxiang" => "934416194@qq.com" } # 作者信息
  spec.platform     = :ios, "10.0" # 平台及支持的最低版本

  spec.source       = { :git => "https://gitee.com/snilwx/module1-framework.git", :tag => "#{spec.version}" } #仓库地址
  
  #spec.dependency 'AFNetworking', '~> 3.0'
  #spec.dependency 'XMNetworking', '~> 1.1.0'
  #spec.dependency 'PodsLibDemo'
  
  #此处为需要添加的 framework 为动态库
  spec.vendored_framework = 'ProjDemo1.framework'

  spec.requires_arc = true #是否支持ARC

end
